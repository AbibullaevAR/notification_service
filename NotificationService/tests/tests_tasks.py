from datetime import datetime

from django.core import mail
from django.test import TestCase
from django.utils.timezone import make_aware

from NotificationService.models import Distribution, Message, Client, MobileCode, Tag
from NotificationService.tasks import start_distribution, SendRequestTask, send_stats_letter

# Create your tests here.


class CeleryTaskTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        tag = Tag.objects.create(name='test')
        mobile_code = MobileCode.objects.create(mobile_code='+7volna')
        [Distribution.objects.create(
            start_date=make_aware(datetime(2022, 8, 12)),
            end_date=make_aware(datetime(2022, 10, 21)),
            start_availability_time='09:30',
            end_availability_time='22:30',
            mobile_code=mobile_code,
            tag=tag
        ) for _ in range(3)]
        Client.objects.create(mobile_number='70987654321', time_zone='Europe/Paris', mobile_code=mobile_code, tag=tag)

    def test_start_distribution_task(self):
        task = start_distribution.s(Distribution.objects.first().pk).apply()

        self.assertEqual(task.status, 'SUCCESS')

    def test_send_request_task(self):
        task = SendRequestTask.s(Distribution.objects.first().pk, Client.objects.first().pk).apply()

        self.assertEqual(task.status, 'SUCCESS')
        self.assertIsNotNone(Message.objects.first())

    def test_send_stats_letter(self):
        task = send_stats_letter.s().apply()

        self.assertEqual(task.status, 'SUCCESS')
        self.assertEqual(len(mail.outbox), 1)
