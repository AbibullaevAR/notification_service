from datetime import datetime

from django.urls import reverse
from django.utils.timezone import make_aware
from rest_framework import status
from rest_framework.test import APITestCase

from NotificationService.models import Tag, MobileCode, Distribution
from NotificationService.views import CreateDistribution


class DistributionViewsTest(APITestCase):

    def test_create_distribution(self):
        tag_name = 'test'
        mobile_code = '+5volna'

        response = self.client.post(reverse('create_distribution'), {
            "start_date": make_aware(datetime(2022, 10, 21)),
            "body_message": "hi",
            "end_date": make_aware(datetime(2022, 10, 21)),
            "mobile_code": mobile_code,
            "tag": tag_name,
            "start_availability_time": '09:30',
            "end_availability_time": '10:30'
        })

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.resolver_match.func.__name__, CreateDistribution.as_view().__name__)

        self.assertEqual(Tag.objects.first().name, tag_name)
        self.assertEqual(MobileCode.objects.first().mobile_code, mobile_code)

        self.assertIsNotNone(Distribution.objects.first())

