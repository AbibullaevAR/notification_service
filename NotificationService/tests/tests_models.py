from datetime import datetime

from django.test import TestCase
from django.utils.timezone import make_aware

from NotificationService.models import Tag, MobileCode, Distribution, Client, Message


class DistributionModelTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        tag = Tag.objects.create(name='test')
        mobile_code = MobileCode.objects.create(mobile_code='+7volna')
        distributions = [Distribution.objects.create(
            start_date=make_aware(datetime(2022, 9, 12)),
            end_date=make_aware(datetime(2022, 10, 21)),
            start_availability_time='09:30',
            end_availability_time='22:30',
            mobile_code=mobile_code,
            tag=tag
        ) for _ in range(3)]
        client = Client.objects.create(mobile_number='70987654321', time_zone='utc', mobile_code=mobile_code, tag=tag)
        [
            [Message.objects.create(status=200, distribution=distribution, client=client) for _ in range(3)]
            for distribution in distributions
        ]

    def test_get_total_stats(self):
        total_stats = Distribution.get_total_stats()

        self.assertEqual(
            total_stats[0].total_msg,
            len(Message.objects.filter(distribution=Distribution.objects.first()))
        )
        self.assertEqual(
            total_stats[0].successful_msg,
            len(Message.objects.filter(distribution=Distribution.objects.first(), status=200))
        )

    def test_get_stats_distribution(self):
        stats_distribution = Distribution.get_stats_distribution(Distribution.objects.first().pk)

        self.assertEqual(
            stats_distribution.total_msg,
            len(Message.objects.filter(distribution=Distribution.objects.first()))
        )
        self.assertEqual(
            stats_distribution.successful_msg,
            len(Message.objects.filter(distribution=Distribution.objects.first(), status=200))
        )

