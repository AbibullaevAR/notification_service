import os

from celery import Celery
from celery.schedules import crontab

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Notification_service.settings')

app = Celery('Notification_service', broker=f"redis://{os.environ.get('REDIS_HOST')}:6379/0")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Create different queue:
# `distribution` - for async start distribution
# `external_req` - for async execute request to external service
app.task_routes = {
    'NotificationService.start_distribution': {'queue': 'distribution'},
    'NotificationService.SendRequestTask': {'queue': 'external_req'}
}

app.conf.beat_schedule = {
    # send stats letter every day at 10:00 am
    'send-stats-letter-every-day': {
        'task': 'NotificationService.send_stats_letter',
        'schedule': crontab(minute=0, hour=10),
        'args': ()
    },
}

# Load task modules from all registered Django apps.
app.autodiscover_tasks()

