from datetime import datetime

from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone
from django.db.models import Count, Q

# Create your models here.


class Tag(models.Model):

    name = models.CharField(max_length=50, unique=True)


class MobileCode(models.Model):

    mobile_code = models.CharField(max_length=7, unique=True)


class Distribution(models.Model):

    start_date = models.DateTimeField()
    body_message = models.TextField()
    end_date = models.DateTimeField()

    mobile_code = models.ForeignKey(
        MobileCode,
        on_delete=models.RESTRICT
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.RESTRICT
    )

    start_availability_time = models.TimeField(default=timezone.now)
    end_availability_time = models.TimeField(default=timezone.now)

    @classmethod
    def _apply_annotate(cls, query_set):
        return query_set.annotate(
            successful_msg=Count('message',
                                 filter=Q(message__status=200))
        ).annotate(
            failed_msg=Count('message',
                             filter=Q(message__status=400))
        ).annotate(total_msg=Count('message'))

    @classmethod
    def get_stats_distribution(cls, pk):
        """
        Obtaining detailed statistics of sent messages for a specific distribution.
        :param pk: id distribution
        :return: Distribution object with additional field (successful_msg, failed_msg, total_msg)
        :return:type: Distribution object
        """
        return cls._apply_annotate(cls.objects.filter(pk=pk)).first()

    @classmethod
    def get_total_stats(cls):
        """
        Obtaining general statistics on the created mailing lists and the number of sent messages on them, grouped by status.
        :return: Distribution query set
        :return:type: Distribution query set

        All Distribution object (in query set) has additional field (successful_msg, failed_msg, total_msg)
        """
        return cls._apply_annotate(cls.objects.all())


class Client(models.Model):

    mobile_number = models.CharField(
        max_length=11,
        # regular expression setup number phone in format 7XXXXXXXXXX (X - number in 0 to 9)
        validators=[RegexValidator(r'7\d{10}', 'Not valid phone number')]
    )

    time_zone = models.CharField(max_length=30)

    mobile_code = models.ForeignKey(
        MobileCode,
        on_delete=models.RESTRICT
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.RESTRICT
    )


class Message(models.Model):

    sending_date = models.DateTimeField(auto_now_add=True)

    status = models.IntegerField()

    client = models.ForeignKey(
        Client,
        on_delete=models.RESTRICT
    )

    distribution = models.ForeignKey(
        Distribution,
        on_delete=models.CASCADE
    )

