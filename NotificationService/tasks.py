from datetime import datetime, timedelta
from pytz import timezone

from django.core.mail import send_mail
from django.utils import timezone as django_timezone
from celery import shared_task, group, Task
from requests import post
from django.conf import settings

from .models import Distribution, Client, Message
from .celery import app
from .errors import ExternalServiceError


class SendRequestTask(Task):
    """
    Send request to ExternalService.

    Attributes
    ----------
    name : str
        celery task name
    max_retries : int
        max retry after task was revoke
    url : str
        sample url to do request ExternalService
    headers : dict
        request headers include EXTERNAL_SERVICE_AUTH_TOKEN
    req_data : dict
        sample request data

    Methods
    -------
    before_start(task_id, args, kwargs):
        run before task and check client availability time.
    run(distribution_id, client_id, *args, **kwargs):
        send request to ExternalService.
    """
    # Celery task prams
    name = 'NotificationService.SendRequestTask'
    max_retries = 7
    throws = (ExternalServiceError, )

    # Request params
    url = 'https://probe.fbrq.cloud/v1/send/{msg_id}'
    headers = {
        'accept': 'application/json',
        'Authorization': f'Bearer {settings.EXTERNAL_SERVICE_AUTH_TOKEN}',
        'Content-Type': 'application/json'
    }
    req_data = {
      "id": 1,
      "phone": 0,
      "text": "string"
    }

    def before_start(self, task_id, args, kwargs):
        """
        Check client availability time.

        :param task_id:
        :param args: list where [0] = distribution_id, [1] = client_id
        :param kwargs:
        :return:

        if client not availability delay task to time when client will be availability.
        """
        client_timezone = timezone(Client.objects.get(pk=args[1]).time_zone)
        date_time_local = datetime.now(client_timezone)

        distribution = Distribution.objects.get(pk=args[0])

        if distribution.start_availability_time <= date_time_local.time() < distribution.end_availability_time:
            return

        task_eta_time = datetime.combine(
            date_time_local.date(),
            distribution.start_availability_time
        )

        if date_time_local.time() > distribution.end_availability_time:
            task_eta_time = task_eta_time + timedelta(days=1)

        task_eta_time = client_timezone.localize(task_eta_time)

        self.retry(expires=self.expires, eta=task_eta_time.astimezone(timezone('utc')))

    def run(self, distribution_id, client_id, *args, **kwargs):
        """
        Send request to ExternalService.
        :param distribution_id: distribution id in the context of what do request
        :param client_id: client id to whom the letter is sent
        :param args:
        :param kwargs:
        :return:

        All info about request write in `Message` object in db.
        if request are failed raise `ExternalServiceError`.
        """
        message, _ = Message.objects.get_or_create(
            client_id=client_id,
            distribution_id=distribution_id,
            defaults={
                'status': 0,
                'distribution_id': distribution_id,
                'client_id': client_id
            }
        )

        msg_pk = message.pk

        url = self.url.format(msg_id=msg_pk)
        self.req_data['id'] = msg_pk
        self.req_data['phone'] = Client.objects.get(pk=client_id).mobile_number
        self.req_data['text'] = Distribution.objects.get(pk=distribution_id).body_message

        resp = post(url, headers=self.headers, json=self.req_data)

        message.status = resp.status_code
        message.save()

        if resp.status_code != 200:
            raise ExternalServiceError(resp.status_code)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        self.retry(expires=self.expires, countdown=30, throw=False)


SendRequestTask = app.register_task(SendRequestTask())


@shared_task(bind=True, name='NotificationService.start_distribution')
def start_distribution(self, distribution_id):
    """
    Find all clients to send letter and create task `SendRequestTask` for him in celery queue.
    :param self: task object
    :param distribution_id: id distribution for start
    :return:
    """
    distribution = Distribution.objects.get(pk=distribution_id)

    clients_for_send = Client.objects.filter(
        mobile_code=distribution.mobile_code,
        tag=distribution.tag
    ).all()

    g = group(SendRequestTask.s(distribution_id, client.pk) for client in clients_for_send)

    g.apply_async(
        expires=distribution.end_date,
        queue='external_req'
    )


@shared_task(name='NotificationService.send_stats_letter')
def send_stats_letter():
    """
    Send stats letter on `STATS_EMAIL`.
    :return:

    For work need set `EMAIL_HOST_USER` and `EMAIL_HOST_PASSWORD`.
    """
    distribution_info_format = 'Distribution {pk:<4}| total_msg {total_msg:<4}| successful_msg {successful_msg:<4}| ' \
                               'failed_msg {failed_msg:<4}'

    distributions_stats = '\n'.join([
        distribution_info_format.format(
            pk=distribution.pk,
            total_msg=distribution.total_msg,
            successful_msg=distribution.successful_msg,
            failed_msg=distribution.failed_msg
        )
        for distribution in Distribution.get_total_stats()
    ])

    send_mail(
        subject=f'Total stats for message from {django_timezone.now()}',
        message=distributions_stats,
        from_email='NotificationService@test.com',
        recipient_list=[settings.STATS_EMAIL]
    )
