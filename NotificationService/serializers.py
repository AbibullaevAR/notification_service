from rest_framework import serializers

from .models import Client, Tag, MobileCode, Distribution
from .serializer_fields import SlugRelatedWithCreateObjField


class ClientSerializer(serializers.ModelSerializer):
    tag = SlugRelatedWithCreateObjField(
        queryset=Tag.objects,
        slug_field='name'
    )
    mobile_code = SlugRelatedWithCreateObjField(
        queryset=MobileCode.objects,
        slug_field='mobile_code'
    )

    class Meta:
        model = Client
        fields = ('mobile_number', 'time_zone', 'mobile_code', 'tag')


class DistributionSerializer(serializers.ModelSerializer):
    tag = SlugRelatedWithCreateObjField(
        queryset=Tag.objects,
        slug_field='name'
    )
    mobile_code = SlugRelatedWithCreateObjField(
        queryset=MobileCode.objects,
        slug_field='mobile_code'
    )

    class Meta:
        model = Distribution
        fields = (
            'start_date',
            'body_message',
            'end_date',
            'mobile_code',
            'tag',
            'start_availability_time',
            'end_availability_time'
        )


class StatsDistributionsSerializer(serializers.Serializer):

    pk = serializers.IntegerField()
    total_msg = serializers.IntegerField()
    successful_msg = serializers.IntegerField()
    failed_msg = serializers.IntegerField()
