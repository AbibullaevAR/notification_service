from django.utils.decorators import method_decorator
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import CreateAPIView, UpdateAPIView, DestroyAPIView, ListAPIView, RetrieveAPIView

from .serializers import ClientSerializer, DistributionSerializer, StatsDistributionsSerializer
from .models import Client, Distribution, Message
from .tasks import start_distribution
from .celery import app


# Create your views here.


class CreateClient(CreateAPIView):
    """
    Create Client for send letter
    """
    serializer_class = ClientSerializer


@method_decorator(name='patch', decorator=swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('pk', openapi.IN_QUERY, description="id updated client", type=openapi.FORMAT_INT64)
    ]
))
class UpdateClient(UpdateAPIView):
    serializer_class = ClientSerializer

    def get_object(self):
        return Client.objects.get(pk=self.request.query_params.get('pk'))


@method_decorator(name='delete', decorator=swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('pk', openapi.IN_QUERY, description="id deleted client", type=openapi.FORMAT_INT64)
    ]
))
class DestroyClient(DestroyAPIView):
    serializer_class = ClientSerializer

    def get_object(self):
        return Client.objects.get(pk=self.request.query_params.get('pk'))


class CreateDistribution(CreateAPIView):
    """
    Create new distribution
    """
    serializer_class = DistributionSerializer

    def perform_create(self, serializer):
        """
        Create distribution.

        :param serializer: DistributionSerializer for serialize
        :return: None

        After create distribution in db create new task in celery.
        """
        distribution = serializer.save()

        start_distribution.apply_async(
            (distribution.pk,),
            queue='distribution',
            eta=distribution.start_date,
            expires=distribution.end_date
        )


@method_decorator(name='patch', decorator=swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('pk', openapi.IN_QUERY, description="id updated distribution", type=openapi.FORMAT_INT64)
    ]
))
class UpdateDistribution(UpdateAPIView):
    """
    Update distribution by pk(request param)
    """
    serializer_class = DistributionSerializer

    def perform_update(self, serializer):
        """
        Update distribution.

        :param serializer: DistributionSerializer for serialize
        :return: None

        After update data in db find task in celery queue and terminate.
        Create new task with updated data.
        """
        updated_distribution = serializer.save()
        i = app.control.inspect(('celery@distribution', ))

        # Find task in celery@distribution worker
        task = next((
            task
            for task in i.scheduled()['celery@distribution']
            if task['request']['args'][0] == updated_distribution.pk
        ), None)

        app.control.terminate(task['request']['id'])

        start_distribution.apply_async(
            (updated_distribution.pk,),
            queue='distribution',
            eta=updated_distribution.start_date,
            expires=updated_distribution.end_date
        )

    def get_object(self):
        """
        Find and return Distribution object by pk(request param)
        :return: Distribution object
        """
        return Distribution.objects.get(pk=self.request.query_params.get('pk'))


@method_decorator(name='delete', decorator=swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('pk', openapi.IN_QUERY, description="id deleted distribution", type=openapi.FORMAT_INT64)
    ]
))
class DestroyDistribution(DestroyAPIView):
    serializer_class = DistributionSerializer

    def get_object(self):
        """
        Find and return Distribution object by pk(request param)
        :return: Distribution object
        """
        return Distribution.objects.get(pk=self.request.query_params.get('pk'))


class TotalStatsDistributions(ListAPIView):
    """
    Obtaining general statistics on the created mailing lists and the number of sent messages on them, grouped by status
    """
    serializer_class = StatsDistributionsSerializer

    def get_queryset(self):
        return Distribution.get_total_stats()


@method_decorator(name='get', decorator=swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('pk', openapi.IN_QUERY, description="id specific mailing list", type=openapi.FORMAT_INT64)
    ]
))
class StatsDistribution(RetrieveAPIView):
    """
    Obtaining detailed statistics of sent messages for a specific distribution
    """
    serializer_class = StatsDistributionsSerializer

    def get_object(self):
        return Distribution.get_stats_distribution(self.request.query_params.get('pk'))
