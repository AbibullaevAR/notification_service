
class ExternalServiceError(Exception):
    def __init__(self, status_code):
        self.status_code = status_code
        super(ExternalServiceError, self).__init__(status_code)
