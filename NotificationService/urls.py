from django.urls import path

from .views import CreateClient, UpdateClient, DestroyClient, CreateDistribution, UpdateDistribution, \
    DestroyDistribution, TotalStatsDistributions, StatsDistribution

urlpatterns = [
    path('create_client/', CreateClient.as_view(), name='create_client'),
    path('update_client/', UpdateClient.as_view(), name='update_client'),
    path('destroy_client/', DestroyClient.as_view(), name='update_client'),

    path('create_distribution/', CreateDistribution.as_view(), name='create_distribution'),
    path('update_distribution/', UpdateDistribution.as_view(), name='update_distribution'),
    path('destroy_distribution/', DestroyDistribution.as_view(), name='update_distribution'),

    path('total_stats/', TotalStatsDistributions.as_view(), name='total_stats'),
    path('stats/', StatsDistribution.as_view(), name='stats')
]
