# Notification_service




## Name
Notification_service

## Description
Methods for creating a new mailing list, viewing the created ones and getting statistics on completed mailing lists were implemented. Documentation available at url `http://127.0.0.1:8080/docs/` after run server. (Были выполнены дополнительные пункты: 1, 3, 5, 8, 9, 11)

## Installation
Installation requires docker-compose to be installed. After installation, you need to edit the .env.db and .env.dev files. .env.db file required to set database environment variables.  
File .env.db contains the following variables: 
- `POSTGRES_USER` sets the database user
- `POSTGRES_PASSWORD` sets the database password
- `POSTGRES_DB` sets the database name

File .env.dev contains the following variables:
- `DEBUG` sets DEBUG mode (1 to on and 0 to off mode)
- `SECRET_KEY` sets SECRET_KEY for work django (keep the secret)
- `DJANGO_ALLOWED_HOSTS` sets ALLOWED_HOSTS use `localhost 127.0.0.1 [::1]` for local run
- `DATABASES_NAME` sets the database name for connect to postgres must equals with `POSTGRES_DB` from file .env.db
- `DATABASES_USER` sets the database user for connect to postgres must equals with `POSTGRES_USER` from file .env.db
- `DATABASES_PASSWORD` sets the database password for connect to postgres must equals with `POSTGRES_PASSWORD` from file .env.db
- `DATABASES_HOST` sets the database host for connect to postgres must have value `db`
- `DATABASES_PORT` sets the database port for connect to postgres must have value `5432`
- `REDIS_HOST` sets the host to celery connect broker must have value `cache`
- `REDIS_PORT` sets the port to celery connect broker must have value `6379`
- `EXTERNAL_SERVICE_AUTH_TOKEN` sets token to auth on external service
- `EMAIL_HOST_USER` sets gmail login from which the email with statistics will be sent
- `EMAIL_HOST_PASSWORD` sets gmail password 
- `STATS_EMAIL` sets email to which the email will be sent stats letter

After sets environment variables use `docker-compose up -d --build` command in project directory. Service will be available on `http://127.0.0.1:8080/`.

Documentation available at url `http://127.0.0.1:8080/docs/`.

